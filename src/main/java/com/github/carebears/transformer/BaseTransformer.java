package com.github.carebears.transformer;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public abstract class BaseTransformer<DO, DTO> {

    abstract public DTO toDTO(DO domainObject);

    public DO fromDTO(DTO dtoObject) {
        throw new UnsupportedOperationException("Method not implemented !!!");
    }

    public Collection<DTO> toDTOCollection(Collection<DO> domainObjects) {
        return this.toDTOList(domainObjects);
    }


    public List<DTO> toDTOList(Collection<DO> domainObjects) {
        if (domainObjects == null) {
            return Collections.emptyList();
        }
        return domainObjects.stream().map(this::toDTO).collect(Collectors.toList());
    }

    public Collection<DO> fromDTOCollection(Collection<DTO> dtoObjects) {
        if (dtoObjects == null) {
            return Collections.emptyList();
        }
        return dtoObjects.stream().
                map(this::fromDTO).
                collect(Collectors.toCollection(LinkedList::new));
    }

    public List<DO> fromDTOList(List<DTO> dtoObjects) {
        if (dtoObjects == null) {
            return Collections.emptyList();
        }
        return dtoObjects.stream().
                map(this::fromDTO).
                collect(Collectors.toCollection(LinkedList::new));
    }
}
