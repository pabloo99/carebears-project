package com.github.carebears.transformer.tni;

import com.github.carebears.api.dto.tni.BandDto;
import com.github.carebears.model.tni.Band;
import com.github.carebears.transformer.BaseTransformer;
import org.springframework.stereotype.Component;

@Component
public class BandTransformer  extends BaseTransformer<Band, BandDto> {

    @Override
    public BandDto toDTO(Band domainObject) {
        BandDto item = new BandDto();

        item.setId(domainObject.getId());
        item.setName(domainObject.getName());
        item.setCreationYear(domainObject.getCreationYear());

        return item;
    }

    @Override
    public Band fromDTO(BandDto dto) {
        Band item = new Band();

        item.setId(dto.getId());
        item.setName(dto.getName());
        item.setCreationYear(dto.getCreationYear());

        return item;
    }
}
