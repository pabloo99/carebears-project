package com.github.carebears.transformer.pabloo99;

import com.github.carebears.api.dto.pabloo99.TeamDto;
import com.github.carebears.model.pabloo99.Team;
import com.github.carebears.transformer.BaseTransformer;
import org.springframework.stereotype.Component;

@Component
public class TeamTransformer extends BaseTransformer<Team, TeamDto> {

    @Override
    public TeamDto toDTO(Team domainObject) {
        TeamDto item = new TeamDto();

        item.setId(domainObject.getId());
        item.setCountry(domainObject.getCountry());
        item.setName(domainObject.getName());

        return item;
    }

    @Override
    public Team fromDTO(TeamDto dto) {
        Team item = new Team();

        item.setId(dto.getId());
        item.setCountry(dto.getCountry());
        item.setName(dto.getName());

        return item;
    }
}
