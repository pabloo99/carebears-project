package com.github.carebears.transformer.swr;

import com.github.carebears.api.dto.swr.WeaponDto;
import com.github.carebears.model.swr.Weapon;
import com.github.carebears.transformer.BaseTransformer;
import org.springframework.stereotype.Component;

@Component
public class WeaponTransformer extends BaseTransformer<Weapon, WeaponDto> {

    @Override
    public WeaponDto toDTO(Weapon domainObject) {
        WeaponDto item = new WeaponDto();

        item.setId(domainObject.getId());
        item.setName(domainObject.getName());
        item.setDamage(domainObject.getDamage());
        item.setLevel(domainObject.getLevel());

        return item;
    }

    @Override
    public Weapon fromDTO(WeaponDto dto) {
        Weapon item = new Weapon();

        item.setId(dto.getId());
        item.setName(dto.getName());
        item.setDamage(dto.getDamage());
        item.setLevel(dto.getLevel());

        return item;
    }
}
