package com.github.carebears.transformer.mpo;

import com.github.carebears.api.dto.mpo.TrainDto;
import com.github.carebears.model.mpo.Train;
import com.github.carebears.transformer.BaseTransformer;
import org.springframework.stereotype.Component;

@Component
public class TrainTransformer extends BaseTransformer<Train, TrainDto> {

    @Override
    public TrainDto toDTO(Train domainObject) {
        TrainDto item = new TrainDto();

        item.setId(domainObject.getId());
        item.setName(domainObject.getName());
        item.setCategory(domainObject.getCategory());
        item.setRange(domainObject.getRange());
        item.setDesignation(domainObject.getDesignation());

        return item;
    }

    @Override
    public Train fromDTO(TrainDto dto) {
        Train item = new Train();

        item.setId(dto.getId());
        item.setName(dto.getName());
        item.setCategory(dto.getCategory());
        item.setRange(dto.getRange());
        item.setDesignation(dto.getDesignation());

        return item;
    }
}
