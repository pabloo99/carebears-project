package com.github.carebears;

import com.github.carebears.api.dto.pabloo99.TeamDto;
import com.github.carebears.api.dto.tni.BandDto;
import com.github.carebears.api.service.pabloo99.TeamService;
import com.github.carebears.api.service.tni.BandService;
import com.github.carebears.configuration.AppConfig;
import com.github.carebears.dao.pabloo99.TeamDao;
import com.github.carebears.facade.pabloo99.TeamFacade;
import com.github.carebears.facade.tni.BandFacade;
import com.github.carebears.model.pabloo99.Team;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.UUID;

public class Main {

    public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);

        UUID uuid = UUID.randomUUID();
        String id = uuid.toString();

        TeamDto dto = new TeamDto();
        dto.setId(id);
        dto.setName("FC Barcelona");
        dto.setCountry("Spain");

        TeamService teamService = (TeamService) context.getBean("teamService");

        teamService.save(dto);

        TeamFacade teamFacade = (TeamFacade) context.getBean("teamFacade");

        teamFacade.findAll();

        BandDto dto1 = new BandDto();
        dto1.setId(id);
        dto1.setName("Grammatik");
        dto1.setCreationYear(1997);

        BandService bandService = (BandService) context.getBean("bandService");
        bandService.save(dto1);

        BandFacade bandFacade = (BandFacade) context.getBean("bandFacade");

        bandFacade.findAll();
    }
}
