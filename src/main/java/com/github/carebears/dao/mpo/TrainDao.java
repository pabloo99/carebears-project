package com.github.carebears.dao.mpo;

import com.github.carebears.model.mpo.Train;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("trainDao")
@Transactional
public class TrainDao extends HibernateDaoSupport {

    @Autowired
    public void init(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    public void save(Train train) {
        getHibernateTemplate().save(train);
    }

    public void update(Train train) {
        getHibernateTemplate().update(train);
    }

    public void delete(Train train) {
        getHibernateTemplate().delete(train);
    }

    public List<Train> findAll() {
        DetachedCriteria criteria = DetachedCriteria.forClass(Train.class);
        return (List<Train>) getHibernateTemplate().findByCriteria(criteria);
    }
}
