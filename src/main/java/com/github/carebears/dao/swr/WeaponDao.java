package com.github.carebears.dao.swr;

import com.github.carebears.model.swr.Weapon;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("weaponDao")
@Transactional
public class WeaponDao extends HibernateDaoSupport {

    @Autowired
    public void init(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    public void save(Weapon weapon) {
        getHibernateTemplate().save(weapon);
    }

    public void update(Weapon weapon) {
        getHibernateTemplate().update(weapon);
    }

    public void delete(Weapon weapon) {
        getHibernateTemplate().delete(weapon);
    }

    public List<Weapon> findAll() {
        DetachedCriteria criteria = DetachedCriteria.forClass(Weapon.class);
        return (List<Weapon>) getHibernateTemplate().findByCriteria(criteria);
    }
}
