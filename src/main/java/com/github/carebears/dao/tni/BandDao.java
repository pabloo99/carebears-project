package com.github.carebears.dao.tni;

import com.github.carebears.model.tni.Band;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("bandDao")
@Transactional
public class BandDao extends HibernateDaoSupport {

    @Autowired
    public void init(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    public void save(Band band) {
        getHibernateTemplate().save(band);
    }

    public void update(Band band) {
        getHibernateTemplate().update(band);
    }

    public void delete(Band band) {
        getHibernateTemplate().delete(band);
    }

    public List<Band> findAll() {
        DetachedCriteria criteria = DetachedCriteria.forClass(Band.class);
        return (List<Band>) getHibernateTemplate().findByCriteria(criteria);
    }
}
