package com.github.carebears.dao.pabloo99;

import com.github.carebears.model.pabloo99.Team;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("teamDao")
@Transactional
public class TeamDao extends HibernateDaoSupport {

    @Autowired
    public void init(SessionFactory sessionFactory) {
        setSessionFactory(sessionFactory);
    }

    public void save(Team team) {
        getHibernateTemplate().save(team);
    }

    public void update(Team team) {
        getHibernateTemplate().update(team);
    }

    public void delete(Team team) {
        getHibernateTemplate().delete(team);
    }

    public List<Team> findAll() {
        DetachedCriteria criteria = DetachedCriteria.forClass(Team.class);
        return (List<Team>) getHibernateTemplate().findByCriteria(criteria);
    }
}
