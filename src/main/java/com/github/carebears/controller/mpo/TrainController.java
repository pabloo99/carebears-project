package com.github.carebears.controller.mpo;

import com.github.carebears.api.dto.mpo.TrainDto;
import com.github.carebears.api.service.mpo.TrainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class TrainController {

    private final TrainService trainService;

    @Autowired
    public TrainController(TrainService trainService) {
        this.trainService = trainService;
    }

    @RequestMapping("/trains")
    public List<TrainDto> trains() {
        return trainService.findAll();
    }
}
