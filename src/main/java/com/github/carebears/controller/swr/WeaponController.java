package com.github.carebears.controller.swr;

import com.github.carebears.api.dto.swr.WeaponDto;
import com.github.carebears.api.service.swr.WeaponService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class WeaponController {

    private final WeaponService weaponService;

    @Autowired
    public WeaponController(WeaponService weaponService) {
        this.weaponService = weaponService;
    }

    @RequestMapping("/weapons")
    public List<WeaponDto> teams() {
        return weaponService.findAll();
    }
}
