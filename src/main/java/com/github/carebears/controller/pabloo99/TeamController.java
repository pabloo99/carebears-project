package com.github.carebears.controller.pabloo99;

import com.github.carebears.api.dto.pabloo99.TeamDto;
import com.github.carebears.api.service.pabloo99.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class TeamController {

    private final TeamService teamService;

    @Autowired
    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    @RequestMapping("/teams")
    public List<TeamDto> teams() {
        return teamService.findAll();
    }
}
