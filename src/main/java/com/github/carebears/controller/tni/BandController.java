package com.github.carebears.controller.tni;

import com.github.carebears.api.dto.tni.BandDto;
import com.github.carebears.api.service.tni.BandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class BandController {

    private final BandService bandService;

    @Autowired
    public BandController(BandService bandService) {
        this.bandService = bandService;
    }

    @RequestMapping("/bands")
    public @ResponseBody List<BandDto> bands() {
        return bandService.findAll();
    }
    @RequestMapping("/addband")
    public @ResponseBody void addBand() {
        bandService.save(new BandDto());
    }
}
