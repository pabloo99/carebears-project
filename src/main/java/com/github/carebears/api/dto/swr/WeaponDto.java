package com.github.carebears.api.dto.swr;

import lombok.Data;

@Data
public class WeaponDto {

    String id;
    String name;
    int damage;
    int level;
}
