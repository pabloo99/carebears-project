package com.github.carebears.api.dto.mpo;

import lombok.Data;
/**
 * Created by Malgorzata.Popiolek on 2017-02-07.
 */

@Data
public class TrainDto {

    private String id;
    private String name;
    private String range;
    private String category;
    private String designation;
}
