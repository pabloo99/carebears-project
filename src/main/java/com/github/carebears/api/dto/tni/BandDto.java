package com.github.carebears.api.dto.tni;

import lombok.Data;

import java.util.UUID;

@Data
public class BandDto {

    private String id;
    private String name;
    private Integer creationYear;

    public BandDto() {
        this.id = UUID.randomUUID().toString();
        this.name = "name";
        this.creationYear = 1;
    }
}
