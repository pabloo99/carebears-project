package com.github.carebears.api.dto.pabloo99;

import lombok.Data;

@Data
public class TeamDto {

    private String id;
    private String name;
    private String country;
}
