package com.github.carebears.api.service.mpo;

import com.github.carebears.api.dto.mpo.TrainDto;

import java.util.List;

public interface TrainService {

    List<TrainDto> findAll();
}
