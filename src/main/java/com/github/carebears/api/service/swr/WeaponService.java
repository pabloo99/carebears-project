package com.github.carebears.api.service.swr;

import com.github.carebears.api.dto.swr.WeaponDto;

import java.util.List;

public interface WeaponService {

    List<WeaponDto> findAll();
}
