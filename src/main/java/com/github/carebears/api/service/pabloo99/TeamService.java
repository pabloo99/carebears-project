package com.github.carebears.api.service.pabloo99;

import com.github.carebears.api.dto.pabloo99.TeamDto;

import java.util.List;

public interface TeamService {

    List<TeamDto> findAll();

    void save(TeamDto team);
}
