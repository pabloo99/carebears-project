package com.github.carebears.api.service.tni;

import com.github.carebears.api.dto.tni.BandDto;
import java.util.List;

public interface BandService {
    List<BandDto> findAll();

    void save(BandDto band);
}


