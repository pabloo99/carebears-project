package com.github.carebears.model.swr;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "SWR_WEAPON")
@Data
public class Weapon implements Serializable {

    @Id
    private String id;

    @NotEmpty
    @Column(name = "NAME", nullable = false)
    private String name;

    @NotEmpty
    @Column(name = "DAMAGE", nullable = false)
    private int damage;

    @NotEmpty
    @Column(name = "LEVEL", nullable = false)
    private int level;
}
