package com.github.carebears.model.pabloo99;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PMA_TEAM")
@Data
public class Team implements Serializable {

    @Id
    private String id;

    @NotEmpty
    @Column(name = "NAME", unique = true, nullable = false)
    private String name;

    @NotEmpty
    @Column(name = "COUNTRY", nullable = false)
    private String country;
}
