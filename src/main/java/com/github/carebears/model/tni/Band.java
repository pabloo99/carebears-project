package com.github.carebears.model.tni;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "TNI_BAND")
@Data
public class Band implements Serializable{

    @Id
    private String id;

    @NotEmpty
    @Column(name = "NAME", nullable = false)
    private String name;

    @NotNull
    @Column(name = "CREATIONYEAR", nullable = false)
    private Integer creationYear;
}
