package com.github.carebears.model.mpo;

import lombok.Data;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "MPO_TRAIN")
@Data
public class Train implements Serializable {

    @Id
    private String id;

    @NotEmpty
    @Column(name = "NAME", unique = true, nullable = false)
    private String name;

    @NotEmpty
    @Column(name = "RANGE", nullable = false)
    private String range;

    @NotEmpty
    @Column(name = "CATEGORY", nullable = false)
    private String category;

    @NotEmpty
    @Column(name = "DESIGNATION", nullable = false)
    private String designation;
}
