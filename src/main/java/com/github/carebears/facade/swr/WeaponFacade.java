package com.github.carebears.facade.swr;

import com.github.carebears.api.dto.swr.WeaponDto;
import com.github.carebears.dao.swr.WeaponDao;
import com.github.carebears.transformer.swr.WeaponTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class WeaponFacade {

    private final WeaponDao weaponDao;
    private final WeaponTransformer weaponTransformer;

    @Autowired
    public WeaponFacade(WeaponDao weaponDao, WeaponTransformer weaponTransformer) {
        this.weaponDao = weaponDao;
        this.weaponTransformer = weaponTransformer;
    }

    public void save(WeaponDto weaponDto) {
        weaponDao.save(weaponTransformer.fromDTO(weaponDto));
    }

    public List<WeaponDto> findAll() {
        return weaponTransformer.toDTOList(weaponDao.findAll());
    }

}
