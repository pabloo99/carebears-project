package com.github.carebears.facade.mpo;

import com.github.carebears.api.dto.mpo.TrainDto;
import com.github.carebears.dao.mpo.TrainDao;
import com.github.carebears.transformer.mpo.TrainTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TrainFacade {

    private final TrainDao trainDao;
    private final TrainTransformer trainTransformer;

    @Autowired
    public TrainFacade(TrainDao trainDao, TrainTransformer trainTransformer) {
        this.trainDao = trainDao;
        this.trainTransformer = trainTransformer;
    }

    public void save(TrainDto trainDto) {
        trainDao.save(trainTransformer.fromDTO(trainDto));
    }

    public List<TrainDto> findAll() {
        return trainTransformer.toDTOList(trainDao.findAll());
    }

}
