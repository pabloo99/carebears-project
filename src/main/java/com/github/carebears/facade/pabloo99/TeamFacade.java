package com.github.carebears.facade.pabloo99;

import com.github.carebears.api.dto.pabloo99.TeamDto;
import com.github.carebears.dao.pabloo99.TeamDao;
import com.github.carebears.transformer.pabloo99.TeamTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TeamFacade {

    private final TeamDao teamDao;
    private final TeamTransformer teamTransformer;

    @Autowired
    public TeamFacade(TeamDao teamDao, TeamTransformer teamTransformer) {
        this.teamDao = teamDao;
        this.teamTransformer = teamTransformer;
    }

    public void save(TeamDto teamDto) {
        teamDao.save(teamTransformer.fromDTO(teamDto));
    }

    public List<TeamDto> findAll() {
        return teamTransformer.toDTOList(teamDao.findAll());
    }

}
