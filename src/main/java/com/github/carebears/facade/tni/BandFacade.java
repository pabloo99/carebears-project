package com.github.carebears.facade.tni;

import com.github.carebears.api.dto.tni.BandDto;
import com.github.carebears.dao.tni.BandDao;
import com.github.carebears.transformer.tni.BandTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

@Component
public class BandFacade {
    private final BandDao bandDao;
    private final BandTransformer bandTransformer;

    @Autowired
    public BandFacade(BandDao bandDao, BandTransformer bandTransformer) {
        this.bandDao = bandDao;
        this.bandTransformer = bandTransformer;
    }

    public void save(BandDto bandDto) {
        bandDao.save(bandTransformer.fromDTO(bandDto));
    }

    public List<BandDto> findAll() {
        return bandTransformer.toDTOList(bandDao.findAll());
    }
}
