package com.github.carebears.service.mpo;

import com.github.carebears.api.dto.mpo.TrainDto;
import com.github.carebears.api.service.mpo.TrainService;
import com.github.carebears.facade.mpo.TrainFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TrainServiceImpl implements TrainService {

    private final TrainFacade trainFacade;

    @Autowired
    public TrainServiceImpl(TrainFacade trainFacade) {
        this.trainFacade = trainFacade;
    }

    @Override
    public List<TrainDto> findAll() {
        return trainFacade.findAll();
    }
}
