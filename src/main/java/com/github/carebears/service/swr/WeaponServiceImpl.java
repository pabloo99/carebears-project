package com.github.carebears.service.swr;

import com.github.carebears.api.dto.swr.WeaponDto;
import com.github.carebears.api.service.swr.WeaponService;
import com.github.carebears.facade.swr.WeaponFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class WeaponServiceImpl implements WeaponService {

    private final WeaponFacade weaponFacade;

    @Autowired
    public WeaponServiceImpl(WeaponFacade weaponFacade) {
        this.weaponFacade = weaponFacade;
    }

    @Override
    public List<WeaponDto> findAll() {
        return weaponFacade.findAll();
    }
}
