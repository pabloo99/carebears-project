package com.github.carebears.service.pabloo99;

import com.github.carebears.api.dto.pabloo99.TeamDto;
import com.github.carebears.api.service.pabloo99.TeamService;
import com.github.carebears.facade.pabloo99.TeamFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("teamService")
public class TeamServiceImpl implements TeamService {

    private final TeamFacade teamFacade;

    @Autowired
    public TeamServiceImpl(TeamFacade teamFacade) {
        this.teamFacade = teamFacade;
    }

    @Override
    public List<TeamDto> findAll() {
        return teamFacade.findAll();
    }

    @Override
    public void save(TeamDto team) {
        teamFacade.save(team);
    }
}
