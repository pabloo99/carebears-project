package com.github.carebears.service.tni;

import com.github.carebears.api.dto.tni.BandDto;
import com.github.carebears.api.service.tni.BandService;
import com.github.carebears.facade.tni.BandFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("bandService")
public class BandServiceImpl implements BandService {
    private final BandFacade bandFacade;

    @Autowired
    public BandServiceImpl(BandFacade bandFacade) {
        this.bandFacade = bandFacade;
    }

    @Override
    public List<BandDto> findAll() {
        return bandFacade.findAll();
    }

    @Override
    public void save(BandDto band) {
        bandFacade.save(band);
    }
}
