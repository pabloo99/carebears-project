package com.github.carebears.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebMvc
@ComponentScan({"com.github.carebears"})

/*@ComponentScan(
        basePackages = {"com.github.carebears"},
        excludeFilters = { @ComponentScan.Filter(type = FilterType.ANNOTATION,value = Configuration.class)})*/
public class AppConfig {
}
